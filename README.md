# Gimbal Bounty Treasury & Escrow (GBTE)

## Current Branches:
1. `main` uses Treasury Address `addr_test1wzn8jnv4dfh7dsufdth0r3r3mljgy7c62084klg7s7jw6wgy3e6yn` on Pre-Production, where `GBTE.TreasuryValidator.hs` does not include `checkReIsOutDat`
2. `addr_test1wpgmsuwjtzh2mcr779fs0pkx4y2xzus4lpd0p7hvj7x7lvsw7qnff` uses Treasury Address `addr_test1wpgmsuwjtzh2mcr779fs0pkx4y2xzus4lpd0p7hvj7x7lvsw7qnff` on Pre-Production, and works in testing on `cardano-cli`, but we need to figure out how to make it work with the [GBTE Front End](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/gbte-front-end). A bounty will be posted for fixing this.
3. `using-addresses` is an implementation of GBTE using `Address` instead of `PubKeyHash`, as demonstrated in [PPBL Course 02, Project 303 Zero One Game](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/ppbl-course-02/-/tree/master/project-303/Zero%20One%20Game). It compiles a `.plutus` script, but has not yet been testing on `cardano-cli`. A bounty will be posted for testing this implementation.

## Contents:
- [Using GBTE on Pre-Production](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/gbte/gbte-plutus/-/blob/master/using-preprod-instance.md)
- [Preparing Tokens](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/gbte/gbte-plutus/-/blob/master/minting-contributor-tokens.md)
- [Scripts for building each transaction](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/gbte/gbte-plutus/-/tree/master/scripts): Use these as documentation to see how this dapp works.

## Roadmap
### 1. Create MVP for use with multi-address browser wallets, and use it to track development of this project
#### 1a. `gbte-plutus`
The GBTE implementation shared in PPBL Summer 2022 uses `PubKeyHash` to identify the Issuer and Contributor roles. While it is possible to use some clever transaction-building on the front end to use the contract as-is, a better solution would be to check the staking credentials of contract participants, as shown in `/ppbl-course-02/project-303/Zero-One-Game`.

- [ ] Re-implement `GBTE.Compiler`, `GBTE.EscrowValidator`, `GBTE.TreasuryValidator` and `GBTE.Types` so that Addresses are used, as demonstrated in the Zero-One Game.
- [ ] Update [documentation/example scripts](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/ppbl-course-02/-/tree/master/project-303/bounty-treasury-escrow) to show how to build `Commit` and `Distribute` transactions in `cardano-cli`, using the new implementation of these contracts.

#### 1b. `gbte-front-end`
Create a simple front end to serve as (a) a working demo of this dapp, and (b) a place to post bounties (on Cardano Mainnet) related to subsequent steps in this roadmap.

### 2. Create instances of GBTE for each 400-Level Project
When we are satisfied with the MVP, create additional GBTE instances for the **Completion Token** and **Dashboard** Projects. We may decide that the next step comes before this one:

### 3. Build GBTE using PlutusV2
Change Contracts so that they compile to PlutusV2 scripts, and write all test transactions for `cardano-cli`. In addition to being the Contracts we'll use to officially release this DApp, they will serve as example material for PPBL Course.

### 4. Create documentation so that anyone can create an instance of GBTE

### 5. Future development
[GBTE Improvement Proposals on Miro](https://miro.com/app/board/uXjVPUwf310=/?share_link_id=338291069131)

#### Component libraries
#### Minting Contributor Token
#### GBTE Instance directories and metadata registration

## Learn
- Review the [project example and PPBL documentation on GitLab](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/ppbl-course-02/-/tree/master/project-303/bounty-treasury-escrow)
- On Canvas: [PPBL Course Module 303]() and [Module 401]()

## License

Apache 2.0
